# -*- coding: utf-8 -*-
from keras.preprocessing.image import ImageDataGenerator

class DogImageGenerator(object):
    def __init__(self, imsize, batch_size):
        self.__imsize = imsize
        self.__batch_size = batch_size
        self.__generator = None
        
    def getGenerator(self, directory, gen_type):
        if gen_type == "train":
            self.__generator = ImageDataGenerator(
                                rescale=1./255,
                                rotation_range=40,
                                width_shift_range=0.2,
                                height_shift_range=0.2,
                                horizontal_flip=True,
                                shear_range=0.1,)
        if gen_type == "val":
            self.__generator = ImageDataGenerator(rescale=1./255)
        return self.__generator.flow_from_directory(
                    directory,
                    target_size=(self.__imsize,self.__imsize), 
                    batch_size=self.__batch_size,
                    class_mode='categorical')

        