# -*- coding: utf-8 -*-
from app import Ui_MainWindow
from cnn import DogNetwork
from PyQt5 import QtCore, QtGui, QtWidgets
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
plt.show()
import sys
import cv2


class MainApp(object):

    def __init__(self):
        self.appl = QtWidgets.QApplication(sys.argv)
        self.start_menu = QtWidgets.QMainWindow()
        self.start_ui = Ui_MainWindow()
        self.start_ui.setupUi(self.start_menu)
        self._net = None
        self._img = None
        self.__connectButtons()
        self.start_menu.show()
        sys.exit(self.appl.exec_())
        
    def __connectButtons(self):
        self.start_ui.loadimage.clicked.connect(self._loadImage)
        self.start_ui.loadnetwork.clicked.connect(self._loadNetwork)
        self.start_ui.predict.clicked.connect(self._predict)
        
    def _loadNetwork(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self.start_menu, "Load pretrained network model", "")
        if fileName:
            self._net = DogNetwork()
            self._net.loadModelFromFile(fileName)
        
    def _loadImage(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self.start_menu, "Load image to get prediction", "")
        if fileName:
            self._imgpath = fileName
            self._img = cv2.imread(fileName)
            self._img = cv2.resize(self._img,(299,299))
        
    def _predict(self):
        breed = self._net.predictBreed(self._img)
        self.start_ui.dog_breed.setText(str(breed))
        plt.axis("off")
        plt.imshow(cv2.cvtColor(self._img, cv2.COLOR_BGR2RGB))
        plt.show()
        
if __name__ == "__main__":
    MainApp()
