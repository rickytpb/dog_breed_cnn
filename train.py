# -*- coding: utf-8 -*-
from __future__ import print_function
import warnings
warnings.filterwarnings('ignore')
import os
os.environ["KERAS_BACKEND"] = "plaidml.keras.backend"
#IF YOU DON'T WANT TO USE PLAIDML BACKEND COMMENT LINE ABOVE
import argparse
import sys
from cnn import DogNetwork

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="CNN Dog Breed Identification Creator")
    parser.add_argument("net_type",help="Choose one of three network types: Xception, InceptionResNetV2, ResNet50")
    parser.add_argument("-epochs", help="Specify epochs for each step like: 15,60,60",default="15,60,60")
    parser.add_argument("-imsize", help="Size for images", default=299)
    parser.add_argument("-batch_size", help="Batch size - use value based on gpu performance", default=32)
    parser.add_argument("-channels", help="Channels from images, color=3, greyscale=1", default=3)
    args = parser.parse_args()
    if args.net_type not in ["Xception","InceptionResNetV2","ResNet50"]:
        print("Select a valid network type. Run train.py --help")
        sys.exit(1)
    epochs = list(map(int,args.epochs.split(',')))
    imsize = int(args.imsize)
    batch_size = int(args.batch_size)
    channels = int(args.channels)
    net = DogNetwork(batch_size,imsize,channels)
    net.createNetwork(args.net_type)
    net.trainNetwork(epochs)
