# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'app.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(376, 301)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayoutWidget = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(9, 9, 351, 131))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.loadimage = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.loadimage.setObjectName("loadimage")
        self.verticalLayout.addWidget(self.loadimage)
        self.loadnetwork = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.loadnetwork.setObjectName("loadnetwork")
        self.verticalLayout.addWidget(self.loadnetwork)
        self.predict = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.predict.setObjectName("predict")
        self.verticalLayout.addWidget(self.predict)
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, 160, 331, 41))
        self.label.setObjectName("label")
        self.dog_breed = QtWidgets.QLabel(self.centralwidget)
        self.dog_breed.setGeometry(QtCore.QRect(20, 210, 331, 41))
        font = QtGui.QFont()
        font.setPointSize(12)
        font.setBold(True)
        font.setWeight(75)
        self.dog_breed.setFont(font)
        self.dog_breed.setText("")
        self.dog_breed.setObjectName("dog_breed")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 376, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Dog Breed Identificator"))
        self.loadimage.setText(_translate("MainWindow", "Load image of a dog"))
        self.loadnetwork.setText(_translate("MainWindow", "Load network model"))
        self.predict.setText(_translate("MainWindow", "Get prediction"))
        self.label.setText(_translate("MainWindow", "Detected breed of dog:"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

