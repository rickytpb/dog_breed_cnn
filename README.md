# Dog breed identificator based on pretrained CNN networks

It's a Convolutional Neural Network application that is able to identify dog breeds by taking a picture as input.  
First unpack data.7z archive so you will have all the data that is mandatory for the app to run.  
If you want to test this app run:  
`python train.py ` 
it's a GUI program that will allow you to upload your own image, pretrained network and predict it's breed.  
If you want to train your own network, first  run:  
`python training.py --help  `
and you will know what to do next  

There is also model summary (txt file) and model summary image.

Stack used:
Keras, PlaidML, Tensorflow GPU, numpy, matplotlib, pandas and other dscience libs.