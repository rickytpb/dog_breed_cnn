# -*- coding: utf-8 -*-
import os 
os.environ["KERAS_BACKEND"] = "plaidml.keras.backend"
import cv2
from keras.models import load_model
from doggenerator import DogImageGenerator
from scipy import ndimage
from keras import applications, metrics, layers, models, regularizers, optimizers
from keras.applications import ResNet50, Xception, InceptionResNetV2
from keras.models import Model
from keras.layers import Dense, Flatten, GlobalAveragePooling2D
from keras.callbacks import ModelCheckpoint, EarlyStopping
import numpy as np
import pandas as pd


class DogNetwork(object):
    
    def __init__(self, batch_size=32, imsize=299,channels=3, temp_model='keras.best.h5'):
        """
        Initialize convolutional neural network with given parameters
        """
        self.__batch_size = batch_size
        self.__imsize = imsize
        self.__channels = channels
        self.__temp_model = temp_model
        self._createGenerators()
        self.__model = None
        
    def _createGenerators(self):
        """
        This method creates generators that are feeding images (and manipulate them)
        to the neural network.
        """
        generator = DogImageGenerator(self.__imsize,self.__batch_size)
        self.__train_gen = generator.getGenerator("data/train","train")
        self.__val_gen = generator.getGenerator("data/val","val")
        
    def createNetwork(self, net_type="Xception", weights='imagenet'):
        """
        Here we create our CNN. You have three base network types and you can define
        number of epochs for each step. You can also set weight source to imagenet 
        or None.
        """
        dimensions = (self.__imsize,self.__imsize,self.__channels)
        if net_type == "Xception":
            base_model = Xception(input_shape=dimensions, 
                                  weights=weights, include_top=False)
        if net_type == "InceptionResNetV2":
            base_model = InceptionResNetV2(input_shape=dimensions, 
                                           weights=weights, include_top=False)
        if net_type == "ResNet50":
            base_model = ResNet50(input_shape=dimensions, 
                                  weights=weights, include_top=False)
            
        for layer in base_model.layers:
            layer.trainable = False
        x = base_model.output
        
        if net_type in ["Xception","InceptionResNetV2"]:
            x = GlobalAveragePooling2D(name='avg_pool')(base_model.output)
            x = Dense(len(self.__train_gen.class_indices), activation='softmax', name='predictions')(x)
        if net_type == "ResNet50":
            x = Flatten()(x)
            x = Dense(len(self.__train_gen.class_indices), activation='softmax', name='predictions')(x)
    
        self.__model = Model(inputs=base_model.input, outputs=x)
        self.__model.compile(loss='categorical_crossentropy',
                optimizer=optimizers.Adam(1e-3),
                metrics=['acc'])
        
    def trainNetwork(self, epochs=[15,60,60]):
        """
        Train complete CNN based on previously selected base network
        and provided number of epochs per step
        """
        callbacks = [ModelCheckpoint(filepath=self.__temp_model, verbose=1, save_best_only=True),
             EarlyStopping(monitor='val_acc', patience=3, verbose=1)]
        self.__model.fit_generator(self.__train_gen,
                    steps_per_epoch=self.__train_gen.samples//self.__batch_size,
                    epochs=epochs[0],
                    validation_data=self.__val_gen,
                    validation_steps=self.__val_gen.samples//self.__batch_size,
                    verbose=1,
                    callbacks=callbacks)
        self.__model.load_weights(self.__temp_model)
        self.__model.compile(optimizer=optimizers.Adam(lr=1e-4,),
                        loss='categorical_crossentropy',
                        metrics=['acc'])
        self.__model.fit_generator(self.__train_gen,
                    steps_per_epoch=self.__train_gen.samples//self.__batch_size,
                    epochs=epochs[1],
                    validation_data=self.__val_gen,
                    validation_steps=self.__val_gen.samples//self.__batch_size,
                    verbose=1,
                    callbacks=callbacks)
        self.__model.load_weights(self.__temp_model)
        for layer in self.__model.layers[:126]:
            layer.trainable = False
        for layer in self.__model.layers[126:]:
            layer.trainable = True
        self.__model.compile(optimizer=optimizers.Adam(lr=1e-4,),
                loss='categorical_crossentropy',
                metrics=['acc'])
        self.__model.fit_generator(self.__train_gen,
                    steps_per_epoch=self.__train_gen.samples//self.__batch_size,
                    epochs=epochs[2],
                    validation_data=self.__val_gen,
                    validation_steps=self.__val_gen.samples//self.__batch_size,
                    verbose=1,
                    callbacks=callbacks)
        
        
    def loadModelFromFile(self,file):
        """
        Load previously saved model from a file
        There is no save method because callbacks automatically save the best network
        """
        self.__model = load_model(file)
        
    def predictBreed(self,img, matrix=False):
        """
        Return string with dog breed for specified image
        If it's a collection of images use matrix = True
        """
        if len(img.shape) == 3:
            test_images = np.asarray([img])
            test_images = np.array(test_images, np.float32) / 255.
        else:
            test_images = img
        pred = self.__model.predict(test_images)
        pred_df = pd.DataFrame(pred)
        if matrix:
            return pred
        return self.getColumns()[pred_df.idxmax(axis=1).values[0]]
        
        
    def getImsize(self):
        """
        Return image size for network
        """
        return self.__imsize
        
    def getColumns(self):
        """
        Get column names from training dataset
        """
        class_indices = sorted([ [k,v] for k, v in self.__train_gen.class_indices.items() ], key=lambda c : c[1])
        return [b[0] for b in class_indices]
        
if __name__ == "__main__":
    """
    net = DogNetwork()
    print("Created")
    net.loadModelFromFile("keras.model.best.h5")
    print("Loaded")
    #print("IMG loaded")
    img = cv2.imread("pes.jpg")
    img = cv2.resize(img,(299,299))
    string = net.predictBreed(img)
    print(string)
    from keras.preprocessing import image

    def load_test_image(fpath):
        img = image.load_img(fpath, target_size=(net.getImsize(), net.getImsize()))
        x = image.img_to_array(img)
        return x

    test_labels = np.loadtxt('data/sample_submission.csv', delimiter=',', dtype=str, skiprows=1)
    test_names = test_labels[:,0]
    predictions = []
    for i, test_name in enumerate(test_names):
        print("%d/%d" % (i,len(test_names)))
        fname = '{}.jpg'.format(test_name)
        data = load_test_image(os.path.join('data/test/', fname))
        predictions.append(net.predictBreed(data))

    columns = net.getColumns()
    df = pd.DataFrame(predictions,columns=columns)
    df = df.assign(id = test_names)
    df.to_csv("submit.csv", index=False,float_format='%.4f')
    print(df.head())
    #print(pred)
    """
        